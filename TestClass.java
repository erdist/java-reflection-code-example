import static org.junit.Assert.*;
import java.lang.reflect.*;

import org.junit.Test;

public class TestClass {

	@Test
	public void test() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException, InvocationTargetException, InstantiationException, NoSuchMethodException {
		
		Object object = Superclass.class.newInstance();
		int counter = object.getClass().getField("counter").getInt(object);
		Method[] methods = object.getClass().getMethods();
		
		for(Method method : methods){
			if(method.getName().equalsIgnoreCase("increment")){
				method.invoke(object, null);
				assertEquals(counter+1, object.getClass().getField("counter").getInt(object));
			}
			counter = object.getClass().getField("counter").getInt(object);
			if(method.getName().equalsIgnoreCase("decrement")){
				method.invoke(object, null);
				assertEquals(counter-1, object.getClass().getField("counter").getInt(object));
			}
			if(method.getName().equalsIgnoreCase("makeZero")){
				method.invoke(object, null);
				assertEquals(0, object.getClass().getField("counter").getInt(object));
			}
			Method x = object.getClass().getMethod("increment", null);
			System.out.println(x.getName());
		}
	}
}
