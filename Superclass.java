
public class Superclass implements Counter{
	public Superclass() {
		// TODO Auto-generated constructor stub
		counter = 0;
	}
	
	@InvokeTimes(invokeCount = 5)
	public void increment(){
		counter++;
	}
	
	public void decrement(){
		counter--;
	}
	
	public void makeZero() {
		counter = 0;
	}
	
	public int counter;
}
