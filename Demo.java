import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.Object;

public class Demo {

	public static void main(String args[]) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException{
		
		Class derivedClass = DerivedClass.class;
		System.out.println(derivedClass.getSimpleName());
		Object object = null;
		
		try {
			object = derivedClass.newInstance();
			Method[] methods = object.getClass().getMethods();
			for(Method method : methods){
				System.out.println(method.getName());
			}
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Field[] fields;
			
		fields = derivedClass.getFields();
		for(Field field : fields){
			System.out.println(field.getName());
		}
		
		System.out.println(object.getClass().getField("counter").getInt(object));
		Method method = object.getClass().getMethod("increment",null);
		
		int invokeTimes =  method.getAnnotation(InvokeTimes.class).invokeCount();
		
		
		for(int i=0;i<invokeTimes;i++){
			method.invoke(object, null);
			System.out.println(object.getClass().getField("counter").getInt(object));
		}
		
		
	}
}
